import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Machine {

	private static Object lock1 = new Object();
	private static Object lock2 = new Object();
	private static volatile int AN; // actual number of users
	private static volatile int AX; // actual number of all tasks

	public static void work(User u) throws InterruptedException {
		addTask(u.getNumberOfTasks());
		addUser();
		final ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(50);

		System.out.println("Actual number of users in proces: " + AN);
		for (int i = 0; i < u.getNumberOfTasks(); i++) {
			newFixedThreadPool.execute(new Worker());

		}

		newFixedThreadPool.shutdown();
		newFixedThreadPool.awaitTermination(10, TimeUnit.MINUTES);
		if (newFixedThreadPool.isTerminated()) {
			System.out.println("ALL TASKS OF USER: " + u.name + " DONE");
			remUser();
		}

	}

	public static void proces() {
		int time;
		Random generator = new Random();
		time = generator.nextInt(2000);

		try {
			Thread.sleep(time);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
		System.out.println("Task done.");

		remTask();
		System.out.println("Number of tasks in process: " + AX);
	}

	private static void addUser() {
		synchronized (lock2) {
			AN += 1;
		}

	}

	private static void remUser() {
		synchronized (lock2) {
			AN -= 1;
		}

	}

	private static void addTask(int a) {
		synchronized (lock1) {
			AX += a;
		}

	}

	public static void remTask() {
		synchronized (lock1) {
			AX -= 1;
		}

	}

}
