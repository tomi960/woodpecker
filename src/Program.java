import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Program {
	public static void main(String[] args) {
		String[] name = { "TOM", "DOM", "BOM", "NOM", "KOM", "ROM" };
		prepare(name);
	}

	public static void prepare(String[] s) {
		ArrayList<User> users = new ArrayList<>();
		final ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(50);
		
		for (int i = 0; i < s.length; i++) {
			User u = new User(s[i], i + 1); // creating new users with different number of tasks
			newFixedThreadPool.execute(u);
			users.add(u);
		}
		
		newFixedThreadPool.shutdown();
		
		try {
			newFixedThreadPool.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (newFixedThreadPool.isTerminated()) {
			for (User u : users) {
				work(u);
			}
		}

	}

	static boolean work(User U) {

		return U.done;
	}

}
