import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class User extends Thread {
	private static int N = 5; // max number of users
	private int Y = 15; // max number of user tasks
	private static int X = 16; // max number of all tasks
	private int NumberOfTasks;
	String name;
	private long W;
	static Semaphore semaphoreU = new Semaphore(N);
	static Semaphore semaphoreT = new Semaphore(X);
	boolean done = false;

	User(String n, int t) {
		name = n;
		this.NumberOfTasks = t;
		Random generator = new Random();
		W = generator.nextInt(2000);

	}

	@Override
	public void run() {

		try {
			if (semaphoreU.tryAcquire(W, TimeUnit.MILLISECONDS)
					&& semaphoreT.tryAcquire(NumberOfTasks, W, TimeUnit.MILLISECONDS) && NumberOfTasks <= Y) {
				Machine.work(this);
				semaphoreT.release(NumberOfTasks);
				semaphoreU.release();
				done = true;

			} else {
				System.out.println("USER: " + name + " FAILED");
			}

		} catch (Exception e) {
			System.out.println("Exception :" + e.getMessage());
		}
	}

	public int getNumberOfTasks() {
		return NumberOfTasks;
	}
}
